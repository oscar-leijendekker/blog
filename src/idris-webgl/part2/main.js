class IdrisError extends Error { }
function __prim_idris2js_FArgList(x){if(x.h === 0){return []}else{return x.a2.concat(__prim_idris2js_FArgList(x.a3))}}
function __prim_js2idris_array(x){if(x.length ===0){return {h:0}}else{return {h:1,a1:x[0],a2: __prim_js2idris_array(x.slice(1))}}}
const __esPrim_idrisworld = (Symbol('idrisworld'));
const __esPrim_int_bound_63 = (BigInt(2) ** BigInt(63) );

function Main_vertexShaderSource(){//main.idr:59:1--59:28
 return 'attribute vec2 aPosition;\n  uniform float xScale;\n  void main() {\n  gl_Position = vec4(aPosition.x * xScale, aPosition.y, 0.0, 1.0);\n  }';
}

function Main_makeTriangle(arg_0, arg_1, arg_2){//main.idr:73:1--73:59
 let imp$5fgen_0;
 switch(arg_1.h){
  case 0: {
   imp$5fgen_0 = arg_1.a1;
   break; }
  default:
   throw new Error('unhandled con case on Prelude/IO.idr:33:1--35:32');
 }
 switch(imp$5fgen_0.h){
  case 0: {
   return imp$5fgen_0.a2(undefined)(undefined)(Main_createShaderProgram(undefined, arg_1, arg_2, Main_vertexShaderSource(), Main_fragmentShaderSource()))((program) => {let imp$5fgen_1;
switch(arg_1.h){
 case 0: {
  imp$5fgen_1 = arg_1.a1;
  break; }
 default:
  throw new Error('unhandled con case on Prelude/IO.idr:33:1--35:32');
}
switch(imp$5fgen_1.h){
 case 0: {
  return imp$5fgen_1.a2(undefined)(undefined)(WebGL2_float32Array(undefined, arg_1, (1n + (1n + (1n + (1n + (1n + (1n + 0n)))))), ({h:1, a1: Prelude_Num_negate$5fNeg$5f$5fDouble(1.0), a2: ({h:1, a1: Prelude_Num_negate$5fNeg$5f$5fDouble(1.0), a2: ({h:1, a1: 1.0, a2: ({h:1, a1: Prelude_Num_negate$5fNeg$5f$5fDouble(1.0), a2: ({h:1, a1: 0.0, a2: ({h:1, a1: 1.0, a2: ({h:0})})})})})})})))((triangleData) => {let imp$5fgen_2;
switch(arg_1.h){
 case 0: {
  imp$5fgen_2 = arg_1.a1;
  break; }
 default:
  throw new Error('unhandled con case on Prelude/IO.idr:33:1--35:32');
}
switch(imp$5fgen_2.h){
 case 0: {
  return imp$5fgen_2.a2(undefined)(undefined)(WebGL2_createBuffer(undefined, arg_1, arg_2))((buffer) => {let imp$5fgen_3;
switch(arg_1.h){
 case 0: {
  imp$5fgen_3 = arg_1.a1;
  break; }
 default:
  throw new Error('unhandled con case on Prelude/IO.idr:33:1--35:32');
}
switch(imp$5fgen_3.h){
 case 0: {
  return imp$5fgen_3.a2(undefined)(undefined)(WebGL2_bindBuffer(undefined, arg_1, arg_2, ({h:0}), buffer))(($5f_827) => {let imp$5fgen_4;
switch(arg_1.h){
 case 0: {
  imp$5fgen_4 = arg_1.a1;
  break; }
 default:
  throw new Error('unhandled con case on Prelude/IO.idr:33:1--35:32');
}
switch(imp$5fgen_4.h){
 case 0: {
  return imp$5fgen_4.a2(undefined)(undefined)(WebGL2_bufferData(undefined, undefined, arg_1, (eta_0) => {return WebGL2_asAnyPtr$5fJSNative$5f$5fFloat32Array(eta_0);}, arg_2, ({h:0}), triangleData, ({h:3})))(($5f_837) => {let imp$5fgen_5;
switch(arg_1.h){
 case 0: {
  imp$5fgen_5 = arg_1.a1;
  break; }
 default:
  throw new Error('unhandled con case on Prelude/IO.idr:33:1--35:32');
}
switch(imp$5fgen_5.h){
 case 0: {
  return imp$5fgen_5.a2(undefined)(undefined)(WebGL2_getAttribLocation(undefined, arg_1, arg_2, program, 'aPosition'))((positionAttribLocation) => {let imp$5fgen_6;
switch(arg_1.h){
 case 0: {
  imp$5fgen_6 = arg_1.a1;
  break; }
 default:
  throw new Error('unhandled con case on Prelude/IO.idr:33:1--35:32');
}
switch(imp$5fgen_6.h){
 case 0: {
  return imp$5fgen_6.a2(undefined)(undefined)(WebGL2_getUniformLocation(undefined, arg_1, arg_2, program, 'xScale'))((scaleUniformLocation) => {let imp$5fgen_7;
switch(arg_1.h){
 case 0: {
  imp$5fgen_7 = arg_1.a1;
  break; }
 default:
  throw new Error('unhandled con case on Prelude/IO.idr:33:1--35:32');
}
let imp$5fgen_8;
switch(imp$5fgen_7.h){
 case 0: {
  imp$5fgen_8 = imp$5fgen_7.a1;
  break; }
 default:
  throw new Error('unhandled con case on Prelude/Interfaces.idr:92:1--104:1');
}
switch(imp$5fgen_8.h){
 case 0: {
  return imp$5fgen_8.a2(undefined)(({h:0, a1: program, a2: buffer, a3: positionAttribLocation, a4: scaleUniformLocation}));
  break; }
 default:
  throw new Error('unhandled con case on Prelude/Interfaces.idr:72:3--72:18');
}});
  break; }
 default:
  throw new Error('unhandled con case on Prelude/Interfaces.idr:95:3--95:35');
}});
  break; }
 default:
  throw new Error('unhandled con case on Prelude/Interfaces.idr:95:3--95:35');
}});
  break; }
 default:
  throw new Error('unhandled con case on Prelude/Interfaces.idr:95:3--95:35');
}});
  break; }
 default:
  throw new Error('unhandled con case on Prelude/Interfaces.idr:95:3--95:35');
}});
  break; }
 default:
  throw new Error('unhandled con case on Prelude/Interfaces.idr:95:3--95:35');
}});
  break; }
 default:
  throw new Error('unhandled con case on Prelude/Interfaces.idr:95:3--95:35');
}});
   break; }
  default:
   throw new Error('unhandled con case on Prelude/Interfaces.idr:95:3--95:35');
 }
}

function Main_main(ext_0){//main.idr:90:1--91:1
 const act_24 = WebGL2_initGL(undefined, ({h:0, a1: ({h:0, a1: ({h:0, a1: (b) => {return (a) => {return (func) => {return (arg_45) => {return (eta_0) => {return Prelude_IO_map$5fFunctor$5f$5fIO(undefined, undefined, func, arg_45, eta_0);};};};};}, a2: (a) => {return (arg_112) => {return (eta_0) => {return arg_112;};};}, a3: (b) => {return (a) => {return (arg_113) => {return (arg_115) => {return (eta_0) => {const act_17 = arg_113(eta_0);
const act_16 = arg_115(eta_0);
return act_17(act_16);};};};};}}), a2: (b) => {return (a) => {return (arg_274) => {return (arg_275) => {return (eta_0) => {const act_24 = arg_274(eta_0);
return arg_275(act_24)(eta_0);};};};};}, a3: (a) => {return (arg_277) => {return (eta_0) => {const act_51 = arg_277(eta_0);
return act_51(eta_0);};};}}), a2: (a) => {return (arg_5832) => {return arg_5832;};}}))(ext_0);
 const act_25 = Main_makeTriangle(undefined, ({h:0, a1: ({h:0, a1: ({h:0, a1: (b) => {return (a) => {return (func) => {return (arg_45) => {return (eta_0) => {return Prelude_IO_map$5fFunctor$5f$5fIO(undefined, undefined, func, arg_45, eta_0);};};};};}, a2: (a) => {return (arg_112) => {return (eta_0) => {return arg_112;};};}, a3: (b) => {return (a) => {return (arg_113) => {return (arg_115) => {return (eta_0) => {const act_17 = arg_113(eta_0);
const act_16 = arg_115(eta_0);
return act_17(act_16);};};};};}}), a2: (b) => {return (a) => {return (arg_274) => {return (arg_275) => {return (eta_0) => {const act_25 = arg_274(eta_0);
return arg_275(act_25)(eta_0);};};};};}, a3: (a) => {return (arg_277) => {return (eta_0) => {const act_51 = arg_277(eta_0);
return act_51(eta_0);};};}}), a2: (a) => {return (arg_5832) => {return arg_5832;};}}), act_24)(ext_0);
 return JSFFI_requestAnimationFrame(undefined, ({h:0, a1: ({h:0, a1: ({h:0, a1: (b) => {return (a) => {return (func) => {return (arg_45) => {return (eta_0) => {return Prelude_IO_map$5fFunctor$5f$5fIO(undefined, undefined, func, arg_45, eta_0);};};};};}, a2: (a) => {return (arg_112) => {return (eta_0) => {return arg_112;};};}, a3: (b) => {return (a) => {return (arg_113) => {return (arg_115) => {return (eta_0) => {const act_17 = arg_113(eta_0);
const act_16 = arg_115(eta_0);
return act_17(act_16);};};};};}}), a2: (b) => {return (a) => {return (arg_274) => {return (arg_275) => {return (eta_0) => {const act_26 = arg_274(eta_0);
return arg_275(act_26)(eta_0);};};};};}, a3: (a) => {return (arg_277) => {return (eta_0) => {const act_51 = arg_277(eta_0);
return act_51(eta_0);};};}}), a2: (a) => {return (arg_5832) => {return arg_5832;};}}), (eta_0) => {return (eta_1) => {return Main_loop(act_24, act_25, eta_0, eta_1);};})(ext_0);
}

function Main_loop(arg_0, arg_1, arg_2, ext_0){//main.idr:28:1--29:1
 WebGL2_clearColor(undefined, ({h:0, a1: ({h:0, a1: ({h:0, a1: (b) => {return (a) => {return (func) => {return (arg_45) => {return (eta_0) => {return Prelude_IO_map$5fFunctor$5f$5fIO(undefined, undefined, func, arg_45, eta_0);};};};};}, a2: (a) => {return (arg_112) => {return (eta_0) => {return arg_112;};};}, a3: (b) => {return (a) => {return (arg_113) => {return (arg_115) => {return (eta_0) => {const act_17 = arg_113(eta_0);
const act_16 = arg_115(eta_0);
return act_17(act_16);};};};};}}), a2: (b) => {return (a) => {return (arg_274) => {return (arg_275) => {return (eta_0) => {const act_24 = arg_274(eta_0);
return arg_275(act_24)(eta_0);};};};};}, a3: (a) => {return (arg_277) => {return (eta_0) => {const act_51 = arg_277(eta_0);
return act_51(eta_0);};};}}), a2: (a) => {return (arg_5832) => {return arg_5832;};}}), arg_0, 0.0, 0.0, 0.0, 1.0)(ext_0);
 WebGL2_clear(undefined, ({h:0, a1: ({h:0, a1: ({h:0, a1: (b) => {return (a) => {return (func) => {return (arg_45) => {return (eta_0) => {return Prelude_IO_map$5fFunctor$5f$5fIO(undefined, undefined, func, arg_45, eta_0);};};};};}, a2: (a) => {return (arg_112) => {return (eta_0) => {return arg_112;};};}, a3: (b) => {return (a) => {return (arg_113) => {return (arg_115) => {return (eta_0) => {const act_17 = arg_113(eta_0);
const act_16 = arg_115(eta_0);
return act_17(act_16);};};};};}}), a2: (b) => {return (a) => {return (arg_274) => {return (arg_275) => {return (eta_0) => {const act_25 = arg_274(eta_0);
return arg_275(act_25)(eta_0);};};};};}, a3: (a) => {return (arg_277) => {return (eta_0) => {const act_51 = arg_277(eta_0);
return act_51(eta_0);};};}}), a2: (a) => {return (arg_5832) => {return arg_5832;};}}), arg_0)(ext_0);
 Main_drawTriangle(arg_0, arg_1, arg_2, ext_0);
 return JSFFI_requestAnimationFrame(undefined, ({h:0, a1: ({h:0, a1: ({h:0, a1: (b) => {return (a) => {return (func) => {return (arg_45) => {return (eta_0) => {return Prelude_IO_map$5fFunctor$5f$5fIO(undefined, undefined, func, arg_45, eta_0);};};};};}, a2: (a) => {return (arg_112) => {return (eta_0) => {return arg_112;};};}, a3: (b) => {return (a) => {return (arg_113) => {return (arg_115) => {return (eta_0) => {const act_17 = arg_113(eta_0);
const act_16 = arg_115(eta_0);
return act_17(act_16);};};};};}}), a2: (b) => {return (a) => {return (arg_274) => {return (arg_275) => {return (eta_0) => {const act_27 = arg_274(eta_0);
return arg_275(act_27)(eta_0);};};};};}, a3: (a) => {return (arg_277) => {return (eta_0) => {const act_51 = arg_277(eta_0);
return act_51(eta_0);};};}}), a2: (a) => {return (arg_5832) => {return arg_5832;};}}), (eta_0) => {return (eta_1) => {return Main_loop(arg_0, arg_1, eta_0, eta_1);};})(ext_0);
}

function Main_fragmentShaderSource(){//main.idr:67:1--67:30
 return 'void main() {\n    gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0);\n  }';
}

function Main_drawTriangle(arg_0, arg_1, arg_2, ext_0){//main.idr:15:1--16:1
 let imp$5fgen_9;
 switch(arg_1.h){
  case 0: {
   imp$5fgen_9 = arg_1.a1;
   break; }
  default:
   throw new Error('unhandled con case on main.idr:8:1--15:1');
 }
 WebGL2_useProgram(undefined, ({h:0, a1: ({h:0, a1: ({h:0, a1: (b) => {return (a) => {return (func) => {return (arg_45) => {return (eta_0) => {return Prelude_IO_map$5fFunctor$5f$5fIO(undefined, undefined, func, arg_45, eta_0);};};};};}, a2: (a) => {return (arg_112) => {return (eta_0) => {return arg_112;};};}, a3: (b) => {return (a) => {return (arg_113) => {return (arg_115) => {return (eta_0) => {const act_17 = arg_113(eta_0);
const act_16 = arg_115(eta_0);
return act_17(act_16);};};};};}}), a2: (b) => {return (a) => {return (arg_274) => {return (arg_275) => {return (eta_0) => {const act_24 = arg_274(eta_0);
return arg_275(act_24)(eta_0);};};};};}, a3: (a) => {return (arg_277) => {return (eta_0) => {const act_51 = arg_277(eta_0);
return act_51(eta_0);};};}}), a2: (a) => {return (arg_5832) => {return arg_5832;};}}), arg_0, imp$5fgen_9)(ext_0);
 let imp$5fgen_10;
 switch(arg_1.h){
  case 0: {
   imp$5fgen_10 = arg_1.a3;
   break; }
  default:
   throw new Error('unhandled con case on main.idr:8:1--15:1');
 }
 WebGL2_enableVertexAttribArray(undefined, ({h:0, a1: ({h:0, a1: ({h:0, a1: (b) => {return (a) => {return (func) => {return (arg_45) => {return (eta_0) => {return Prelude_IO_map$5fFunctor$5f$5fIO(undefined, undefined, func, arg_45, eta_0);};};};};}, a2: (a) => {return (arg_112) => {return (eta_0) => {return arg_112;};};}, a3: (b) => {return (a) => {return (arg_113) => {return (arg_115) => {return (eta_0) => {const act_17 = arg_113(eta_0);
const act_16 = arg_115(eta_0);
return act_17(act_16);};};};};}}), a2: (b) => {return (a) => {return (arg_274) => {return (arg_275) => {return (eta_0) => {const act_25 = arg_274(eta_0);
return arg_275(act_25)(eta_0);};};};};}, a3: (a) => {return (arg_277) => {return (eta_0) => {const act_51 = arg_277(eta_0);
return act_51(eta_0);};};}}), a2: (a) => {return (arg_5832) => {return arg_5832;};}}), arg_0, imp$5fgen_10)(ext_0);
 let imp$5fgen_11;
 switch(arg_1.h){
  case 0: {
   imp$5fgen_11 = arg_1.a2;
   break; }
  default:
   throw new Error('unhandled con case on main.idr:8:1--15:1');
 }
 WebGL2_bindBuffer(undefined, ({h:0, a1: ({h:0, a1: ({h:0, a1: (b) => {return (a) => {return (func) => {return (arg_45) => {return (eta_0) => {return Prelude_IO_map$5fFunctor$5f$5fIO(undefined, undefined, func, arg_45, eta_0);};};};};}, a2: (a) => {return (arg_112) => {return (eta_0) => {return arg_112;};};}, a3: (b) => {return (a) => {return (arg_113) => {return (arg_115) => {return (eta_0) => {const act_17 = arg_113(eta_0);
const act_16 = arg_115(eta_0);
return act_17(act_16);};};};};}}), a2: (b) => {return (a) => {return (arg_274) => {return (arg_275) => {return (eta_0) => {const act_26 = arg_274(eta_0);
return arg_275(act_26)(eta_0);};};};};}, a3: (a) => {return (arg_277) => {return (eta_0) => {const act_51 = arg_277(eta_0);
return act_51(eta_0);};};}}), a2: (a) => {return (arg_5832) => {return arg_5832;};}}), arg_0, ({h:0}), imp$5fgen_11)(ext_0);
 let imp$5fgen_12;
 switch(arg_1.h){
  case 0: {
   imp$5fgen_12 = arg_1.a3;
   break; }
  default:
   throw new Error('unhandled con case on main.idr:8:1--15:1');
 }
 WebGL2_vertexAttribPointer(undefined, ({h:0, a1: ({h:0, a1: ({h:0, a1: (b) => {return (a) => {return (func) => {return (arg_45) => {return (eta_0) => {return Prelude_IO_map$5fFunctor$5f$5fIO(undefined, undefined, func, arg_45, eta_0);};};};};}, a2: (a) => {return (arg_112) => {return (eta_0) => {return arg_112;};};}, a3: (b) => {return (a) => {return (arg_113) => {return (arg_115) => {return (eta_0) => {const act_17 = arg_113(eta_0);
const act_16 = arg_115(eta_0);
return act_17(act_16);};};};};}}), a2: (b) => {return (a) => {return (arg_274) => {return (arg_275) => {return (eta_0) => {const act_27 = arg_274(eta_0);
return arg_275(act_27)(eta_0);};};};};}, a3: (a) => {return (arg_277) => {return (eta_0) => {const act_51 = arg_277(eta_0);
return act_51(eta_0);};};}}), a2: (a) => {return (arg_5832) => {return arg_5832;};}}), arg_0, imp$5fgen_12, 2n, ({h:5}), 1n, 0n, 0n)(ext_0);
 const act_28 = WebGL2_float32Array(undefined, ({h:0, a1: ({h:0, a1: ({h:0, a1: (b) => {return (a) => {return (func) => {return (arg_45) => {return (eta_0) => {return Prelude_IO_map$5fFunctor$5f$5fIO(undefined, undefined, func, arg_45, eta_0);};};};};}, a2: (a) => {return (arg_112) => {return (eta_0) => {return arg_112;};};}, a3: (b) => {return (a) => {return (arg_113) => {return (arg_115) => {return (eta_0) => {const act_17 = arg_113(eta_0);
const act_16 = arg_115(eta_0);
return act_17(act_16);};};};};}}), a2: (b) => {return (a) => {return (arg_274) => {return (arg_275) => {return (eta_0) => {const act_28 = arg_274(eta_0);
return arg_275(act_28)(eta_0);};};};};}, a3: (a) => {return (arg_277) => {return (eta_0) => {const act_51 = arg_277(eta_0);
return act_51(eta_0);};};}}), a2: (a) => {return (arg_5832) => {return arg_5832;};}}), (1n + 0n), ({h:1, a1: Prelude_Types_sin(Prelude_Num_$2f$5fFractional$5f$5fDouble(arg_2, 200.0)), a2: ({h:0})}))(ext_0);
 let imp$5fgen_13;
 switch(arg_1.h){
  case 0: {
   imp$5fgen_13 = arg_1.a4;
   break; }
  default:
   throw new Error('unhandled con case on main.idr:8:1--15:1');
 }
 WebGL2_uniform1fv(undefined, ({h:0, a1: ({h:0, a1: ({h:0, a1: (b) => {return (a) => {return (func) => {return (arg_45) => {return (eta_0) => {return Prelude_IO_map$5fFunctor$5f$5fIO(undefined, undefined, func, arg_45, eta_0);};};};};}, a2: (a) => {return (arg_112) => {return (eta_0) => {return arg_112;};};}, a3: (b) => {return (a) => {return (arg_113) => {return (arg_115) => {return (eta_0) => {const act_17 = arg_113(eta_0);
const act_16 = arg_115(eta_0);
return act_17(act_16);};};};};}}), a2: (b) => {return (a) => {return (arg_274) => {return (arg_275) => {return (eta_0) => {const act_29 = arg_274(eta_0);
return arg_275(act_29)(eta_0);};};};};}, a3: (a) => {return (arg_277) => {return (eta_0) => {const act_51 = arg_277(eta_0);
return act_51(eta_0);};};}}), a2: (a) => {return (arg_5832) => {return arg_5832;};}}), arg_0, imp$5fgen_13, act_28, 0n, 1n)(ext_0);
 return WebGL2_drawArrays(undefined, ({h:0, a1: ({h:0, a1: ({h:0, a1: (b) => {return (a) => {return (func) => {return (arg_45) => {return (eta_0) => {return Prelude_IO_map$5fFunctor$5f$5fIO(undefined, undefined, func, arg_45, eta_0);};};};};}, a2: (a) => {return (arg_112) => {return (eta_0) => {return arg_112;};};}, a3: (b) => {return (a) => {return (arg_113) => {return (arg_115) => {return (eta_0) => {const act_17 = arg_113(eta_0);
const act_16 = arg_115(eta_0);
return act_17(act_16);};};};};}}), a2: (b) => {return (a) => {return (arg_274) => {return (arg_275) => {return (eta_0) => {const act_30 = arg_274(eta_0);
return arg_275(act_30)(eta_0);};};};};}, a3: (a) => {return (arg_277) => {return (eta_0) => {const act_51 = arg_277(eta_0);
return act_51(eta_0);};};}}), a2: (a) => {return (arg_5832) => {return arg_5832;};}}), arg_0, ({h:6}), 0n, 3n)(ext_0);
}

function Main_createShaderProgram(arg_0, arg_1, arg_2, arg_3, arg_4){//main.idr:46:1--46:94
 let imp$5fgen_14;
 switch(arg_1.h){
  case 0: {
   imp$5fgen_14 = arg_1.a1;
   break; }
  default:
   throw new Error('unhandled con case on Prelude/IO.idr:33:1--35:32');
 }
 switch(imp$5fgen_14.h){
  case 0: {
   return imp$5fgen_14.a2(undefined)(undefined)(Main_createShaderFromSource(undefined, arg_1, arg_2, ({h:0}), arg_3))((vertexShader) => {let imp$5fgen_15;
switch(arg_1.h){
 case 0: {
  imp$5fgen_15 = arg_1.a1;
  break; }
 default:
  throw new Error('unhandled con case on Prelude/IO.idr:33:1--35:32');
}
switch(imp$5fgen_15.h){
 case 0: {
  return imp$5fgen_15.a2(undefined)(undefined)(Main_createShaderFromSource(undefined, arg_1, arg_2, ({h:1}), arg_4))((fragmentShader) => {let imp$5fgen_16;
switch(arg_1.h){
 case 0: {
  imp$5fgen_16 = arg_1.a1;
  break; }
 default:
  throw new Error('unhandled con case on Prelude/IO.idr:33:1--35:32');
}
switch(imp$5fgen_16.h){
 case 0: {
  return imp$5fgen_16.a2(undefined)(undefined)(WebGL2_createProgram(undefined, arg_1, arg_2))((program) => {let imp$5fgen_17;
switch(arg_1.h){
 case 0: {
  imp$5fgen_17 = arg_1.a1;
  break; }
 default:
  throw new Error('unhandled con case on Prelude/IO.idr:33:1--35:32');
}
switch(imp$5fgen_17.h){
 case 0: {
  return imp$5fgen_17.a2(undefined)(undefined)(WebGL2_attachShader(undefined, arg_1, arg_2, program, vertexShader))(($5f_683) => {let imp$5fgen_18;
switch(arg_1.h){
 case 0: {
  imp$5fgen_18 = arg_1.a1;
  break; }
 default:
  throw new Error('unhandled con case on Prelude/IO.idr:33:1--35:32');
}
switch(imp$5fgen_18.h){
 case 0: {
  return imp$5fgen_18.a2(undefined)(undefined)(WebGL2_attachShader(undefined, arg_1, arg_2, program, fragmentShader))(($5f_692) => {let imp$5fgen_19;
switch(arg_1.h){
 case 0: {
  imp$5fgen_19 = arg_1.a1;
  break; }
 default:
  throw new Error('unhandled con case on Prelude/IO.idr:33:1--35:32');
}
switch(imp$5fgen_19.h){
 case 0: {
  return imp$5fgen_19.a2(undefined)(undefined)(WebGL2_linkProgram(undefined, arg_1, arg_2, program))(($5f_701) => {let imp$5fgen_20;
switch(arg_1.h){
 case 0: {
  imp$5fgen_20 = arg_1.a1;
  break; }
 default:
  throw new Error('unhandled con case on Prelude/IO.idr:33:1--35:32');
}
switch(imp$5fgen_20.h){
 case 0: {
  return imp$5fgen_20.a2(undefined)(undefined)(WebGL2_getProgramInfoLog(undefined, arg_1, arg_2, program))((errorMsg) => {let imp$5fgen_21;
switch(arg_1.h){
 case 0: {
  imp$5fgen_21 = arg_1.a1;
  break; }
 default:
  throw new Error('unhandled con case on Prelude/IO.idr:33:1--35:32');
}
switch(imp$5fgen_21.h){
 case 0: {
  return imp$5fgen_21.a2(undefined)(undefined)(JSFFI_consoleLog(undefined, undefined, arg_1, (eta_0) => {return JSFFI_asAnyPtr$5fJSNative$5f$5fString(eta_0);}, errorMsg))(($5f_719) => {let imp$5fgen_22;
switch(arg_1.h){
 case 0: {
  imp$5fgen_22 = arg_1.a1;
  break; }
 default:
  throw new Error('unhandled con case on Prelude/IO.idr:33:1--35:32');
}
let imp$5fgen_23;
switch(imp$5fgen_22.h){
 case 0: {
  imp$5fgen_23 = imp$5fgen_22.a1;
  break; }
 default:
  throw new Error('unhandled con case on Prelude/Interfaces.idr:92:1--104:1');
}
switch(imp$5fgen_23.h){
 case 0: {
  return imp$5fgen_23.a2(undefined)(program);
  break; }
 default:
  throw new Error('unhandled con case on Prelude/Interfaces.idr:72:3--72:18');
}});
  break; }
 default:
  throw new Error('unhandled con case on Prelude/Interfaces.idr:95:3--95:35');
}});
  break; }
 default:
  throw new Error('unhandled con case on Prelude/Interfaces.idr:95:3--95:35');
}});
  break; }
 default:
  throw new Error('unhandled con case on Prelude/Interfaces.idr:95:3--95:35');
}});
  break; }
 default:
  throw new Error('unhandled con case on Prelude/Interfaces.idr:95:3--95:35');
}});
  break; }
 default:
  throw new Error('unhandled con case on Prelude/Interfaces.idr:95:3--95:35');
}});
  break; }
 default:
  throw new Error('unhandled con case on Prelude/Interfaces.idr:95:3--95:35');
}});
  break; }
 default:
  throw new Error('unhandled con case on Prelude/Interfaces.idr:95:3--95:35');
}});
   break; }
  default:
   throw new Error('unhandled con case on Prelude/Interfaces.idr:95:3--95:35');
 }
}

function Main_createShaderFromSource(arg_0, arg_1, arg_2, arg_3, arg_4){//main.idr:36:1--36:109
 let imp$5fgen_24;
 switch(arg_1.h){
  case 0: {
   imp$5fgen_24 = arg_1.a1;
   break; }
  default:
   throw new Error('unhandled con case on Prelude/IO.idr:33:1--35:32');
 }
 switch(imp$5fgen_24.h){
  case 0: {
   return imp$5fgen_24.a2(undefined)(undefined)(WebGL2_createShader(undefined, arg_1, arg_2, arg_3))((shader) => {let imp$5fgen_25;
switch(arg_1.h){
 case 0: {
  imp$5fgen_25 = arg_1.a1;
  break; }
 default:
  throw new Error('unhandled con case on Prelude/IO.idr:33:1--35:32');
}
switch(imp$5fgen_25.h){
 case 0: {
  return imp$5fgen_25.a2(undefined)(undefined)(WebGL2_shaderSource(undefined, arg_1, arg_2, shader, arg_4))(($5f_596) => {let imp$5fgen_26;
switch(arg_1.h){
 case 0: {
  imp$5fgen_26 = arg_1.a1;
  break; }
 default:
  throw new Error('unhandled con case on Prelude/IO.idr:33:1--35:32');
}
switch(imp$5fgen_26.h){
 case 0: {
  return imp$5fgen_26.a2(undefined)(undefined)(WebGL2_compileShader(undefined, arg_1, arg_2, shader))(($5f_605) => {let imp$5fgen_27;
switch(arg_1.h){
 case 0: {
  imp$5fgen_27 = arg_1.a1;
  break; }
 default:
  throw new Error('unhandled con case on Prelude/IO.idr:33:1--35:32');
}
switch(imp$5fgen_27.h){
 case 0: {
  return imp$5fgen_27.a2(undefined)(undefined)(WebGL2_getShaderInfoLog(undefined, arg_1, arg_2, shader))((errorMsg) => {let imp$5fgen_28;
switch(arg_1.h){
 case 0: {
  imp$5fgen_28 = arg_1.a1;
  break; }
 default:
  throw new Error('unhandled con case on Prelude/IO.idr:33:1--35:32');
}
switch(imp$5fgen_28.h){
 case 0: {
  return imp$5fgen_28.a2(undefined)(undefined)(JSFFI_consoleLog(undefined, undefined, arg_1, (eta_0) => {return JSFFI_asAnyPtr$5fJSNative$5f$5fString(eta_0);}, errorMsg))(($5f_623) => {let imp$5fgen_29;
switch(arg_1.h){
 case 0: {
  imp$5fgen_29 = arg_1.a1;
  break; }
 default:
  throw new Error('unhandled con case on Prelude/IO.idr:33:1--35:32');
}
let imp$5fgen_30;
switch(imp$5fgen_29.h){
 case 0: {
  imp$5fgen_30 = imp$5fgen_29.a1;
  break; }
 default:
  throw new Error('unhandled con case on Prelude/Interfaces.idr:92:1--104:1');
}
switch(imp$5fgen_30.h){
 case 0: {
  return imp$5fgen_30.a2(undefined)(shader);
  break; }
 default:
  throw new Error('unhandled con case on Prelude/Interfaces.idr:72:3--72:18');
}});
  break; }
 default:
  throw new Error('unhandled con case on Prelude/Interfaces.idr:95:3--95:35');
}});
  break; }
 default:
  throw new Error('unhandled con case on Prelude/Interfaces.idr:95:3--95:35');
}});
  break; }
 default:
  throw new Error('unhandled con case on Prelude/Interfaces.idr:95:3--95:35');
}});
  break; }
 default:
  throw new Error('unhandled con case on Prelude/Interfaces.idr:95:3--95:35');
}});
   break; }
  default:
   throw new Error('unhandled con case on Prelude/Interfaces.idr:95:3--95:35');
 }
}

function Prelude_Types_cast$5fCast$5f$5fNat$5fInt(ext_0){//Prelude/Types.idr:710:1--716:1
 return (ext_0 % __esPrim_int_bound_63);
}

function Prelude_Types_sin(arg_0){//Prelude/Types.idr:591:1--592:23
 return Math.sin(arg_0);
}

function Prelude_Num_negate$5fNeg$5f$5fDouble(arg_0){//Prelude/Num.idr:164:1--169:1
 return (-(arg_0));
}

function Prelude_Num_$2f$5fFractional$5f$5fDouble(ext_0, ext_1){//Prelude/Num.idr:173:1--176:1
 return (ext_0 / ext_1);
}

function PrimIO_case__unsafePerformIO_401(arg_0, arg_1, arg_2, arg_3){//PrimIO.idr:108:32--109:75
 return PrimIO_unsafeDestroyWorld(undefined, undefined, arg_3);
}

function PrimIO_unsafePerformIO(arg_0, arg_1){//PrimIO.idr:105:1--106:36
 return PrimIO_unsafeCreateWorld(undefined, (w) => {return PrimIO_case__unsafePerformIO_401(undefined, undefined, undefined, arg_1(w));});
}

function PrimIO_unsafeDestroyWorld(arg_0, arg_1, arg_2){//PrimIO.idr:102:1--102:46
 return arg_2;
}

function PrimIO_unsafeCreateWorld(arg_0, arg_1){//PrimIO.idr:99:1--99:53
 return arg_1(__esPrim_idrisworld);
}

function Prelude_IO_map$5fFunctor$5f$5fIO(arg_0, arg_1, arg_2, arg_3, ext_0){//Prelude/IO.idr:15:1--19:1
 const act_3 = arg_3(ext_0);
 return arg_2(act_3);
}

function JSFFI_asAnyPtr$5fJSNative$5f$5fString(ext_0){//JSFFI.idr:13:1--17:1
 return JSFFI_prim$5f$5fanyType$5fstring(ext_0);
}

function JSFFI_asAnyPtr$5fJSNative$5f$5fBool(arg_0){//JSFFI.idr:30:1--34:1
 switch(arg_0){
  case 0n: {
   return JSFFI_prim$5f$5fjsTrue();
   break; }
  case 1n: {
   return JSFFI_prim$5f$5fjsFalse();
   break; }
  default:
   throw new Error('unhandled const case on JSFFI.idr:31:10--31:14');
 }
}

function JSFFI_requestAnimationFrame(arg_0, arg_1, arg_2){//JSFFI.idr:43:1--45:1
 switch(arg_1.h){
  case 0: {
   return arg_1.a2(undefined)((eta_0) => {return JSFFI_prim$5f$5frequestAnimationFrame((ptr) => {return arg_2(ptr);}, eta_0);});
   break; }
  default:
   throw new Error('unhandled con case on Prelude/IO.idr:35:3--35:32');
 }
}

const JSFFI_prim$5f$5frequestAnimationFrame = ( callback => {window.requestAnimationFrame(x=>callback(x)())})


const JSFFI_prim$5f$5fjsTrue = ( () => true)


const JSFFI_prim$5f$5fjsFalse = ( () => false)


const JSFFI_prim$5f$5fconsole$5flog = ( msg => console.log(msg))


const JSFFI_prim$5f$5fanyType$5fstring = ( str => str)


function JSFFI_consoleLog(arg_0, arg_1, arg_2, arg_3, arg_4){//JSFFI.idr:34:1--36:1
 switch(arg_2.h){
  case 0: {
   return arg_2.a2(undefined)((eta_0) => {return JSFFI_prim$5f$5fconsole$5flog(arg_3(arg_4), eta_0);});
   break; }
  default:
   throw new Error('unhandled con case on Prelude/IO.idr:35:3--35:32');
 }
}

function WebGL2_n__2567_1957_fill$5fFloat32Array$5frecursive(arg_0, arg_1, arg_2, arg_3, arg_4, arg_5, arg_6){//WebGL2.idr:147:5--148:5
 switch(arg_6.h){
  case 0: {
   let imp$5fgen_31;
   switch(arg_2.h){
    case 0: {
     imp$5fgen_31 = arg_2.a1;
     break; }
    default:
     throw new Error('unhandled con case on Prelude/IO.idr:33:1--35:32');
   }
   let imp$5fgen_32;
   switch(imp$5fgen_31.h){
    case 0: {
     imp$5fgen_32 = imp$5fgen_31.a1;
     break; }
    default:
     throw new Error('unhandled con case on Prelude/Interfaces.idr:92:1--104:1');
   }
   let imp$5fgen_33;
   switch(imp$5fgen_32.h){
    case 0: {
     imp$5fgen_33 = imp$5fgen_32.a2(undefined)(({h:0}));
     break; }
    default:
     throw new Error('unhandled con case on Prelude/Interfaces.idr:72:3--72:18');
   }
   return imp$5fgen_33;
   break; }
  case 1: {
   let imp$5fgen_34;
   switch(arg_2.h){
    case 0: {
     imp$5fgen_34 = arg_2.a1;
     break; }
    default:
     throw new Error('unhandled con case on Prelude/IO.idr:33:1--35:32');
   }
   let imp$5fgen_35;
   switch(imp$5fgen_34.h){
    case 0: {
     let imp$5fgen_36;
     switch(arg_2.h){
      case 0: {
       imp$5fgen_36 = arg_2.a2(undefined)((eta_0) => {return WebGL2_prim$5f$5fset$5fFloat32Array$5fat(arg_3, arg_5, arg_6.a1, eta_0);});
       break; }
      default:
       throw new Error('unhandled con case on Prelude/IO.idr:35:3--35:32');
     }
     imp$5fgen_35 = imp$5fgen_34.a2(undefined)(undefined)(imp$5fgen_36)(($5f_2014) => {return WebGL2_n__2567_1957_fill$5fFloat32Array$5frecursive(undefined, undefined, arg_2, arg_3, undefined, ((arg_5 + 1n) % __esPrim_int_bound_63), arg_6.a2);});
     break; }
    default:
     throw new Error('unhandled con case on Prelude/Interfaces.idr:95:3--95:35');
   }
   return imp$5fgen_35;
   break; }
  default:
   throw new Error('unhandled con case on WebGL2.idr:147:42--147:55');
 }
}

function WebGL2_enumName$5fGLEnum$5f$5fGLShaderType(arg_0){//WebGL2.idr:246:1--253:1
 switch(arg_0.h){
  case 0: {
   return 'VERTEX_SHADER';
   break; }
  case 1: {
   return 'FRAGMENT_SHADER';
   break; }
  default:
   throw new Error('unhandled con case on WebGL2.idr:246:8--246:20');
 }
}

function WebGL2_enumName$5fGLEnum$5f$5fGLBufferUsage(arg_0){//WebGL2.idr:88:1--100:1
 switch(arg_0.h){
  case 0: {
   return 'STREAM_DRAW';
   break; }
  case 1: {
   return 'STREAM_READ';
   break; }
  case 2: {
   return 'STREAM_COPY';
   break; }
  case 3: {
   return 'STATIC_DRAW';
   break; }
  case 4: {
   return 'STATIC_READ';
   break; }
  case 5: {
   return 'STATIC_COPY';
   break; }
  case 6: {
   return 'DYNAMIC_DRAW';
   break; }
  case 7: {
   return 'DYNAMIC_READ';
   break; }
  case 8: {
   return 'DYNAMIC_COPY';
   break; }
  default:
   throw new Error('unhandled con case on WebGL2.idr:88:8--88:21');
 }
}

function WebGL2_enumName$5fGLEnum$5f$5fGLBufferTarget(arg_0){//WebGL2.idr:64:1--76:1
 switch(arg_0.h){
  case 0: {
   return 'ARRAY_BUFFER';
   break; }
  case 1: {
   return 'ELEMENT_ARRAY_BUFFER';
   break; }
  case 2: {
   return 'PIXEL_PACK_BUFFER';
   break; }
  case 3: {
   return 'PIXEL_UNPACK_BUFFER';
   break; }
  case 4: {
   return 'COPY_READ_BUFFER';
   break; }
  case 5: {
   return 'COPY_WRITE_BUFFER';
   break; }
  case 6: {
   return 'TRANSFORM_FEEDBACK_BUFFER';
   break; }
  case 7: {
   return 'UNIFORM_BUFFER';
   break; }
  default:
   throw new Error('unhandled con case on WebGL2.idr:64:8--64:22');
 }
}

function WebGL2_enumName$5fGLEnum$5f$5fDrawMode(arg_0){//WebGL2.idr:523:1--536:1
 switch(arg_0.h){
  case 0: {
   return 'POINTS';
   break; }
  case 1: {
   return 'LINE_STRIP';
   break; }
  case 2: {
   return 'LINE_LOOP';
   break; }
  case 3: {
   return 'LINES';
   break; }
  case 4: {
   return 'TRIANGLE_STRIP';
   break; }
  case 5: {
   return 'TRIANGLE_FAN';
   break; }
  case 6: {
   return 'TRIANGLES';
   break; }
  default:
   throw new Error('unhandled con case on WebGL2.idr:523:8--523:16');
 }
}

function WebGL2_enumName$5fGLEnum$5f$5fAttribType(arg_0){//WebGL2.idr:493:1--502:1
 switch(arg_0.h){
  case 0: {
   return 'BYTE';
   break; }
  case 1: {
   return 'SHORT';
   break; }
  case 2: {
   return 'UNSIGNED_BYTE';
   break; }
  case 3: {
   return 'UNSIGNED_SHORT';
   break; }
  case 4: {
   return 'FIXED';
   break; }
  case 5: {
   return 'FLOAT';
   break; }
  default:
   throw new Error('unhandled con case on WebGL2.idr:493:8--493:18');
 }
}

function WebGL2_asAnyPtr$5fJSNative$5f$5fFloat32Array(arg_0){//WebGL2.idr:128:1--132:1
 return arg_0;
}

function WebGL2_vertexAttribPointer(arg_0, arg_1, arg_2, arg_3, arg_4, arg_5, arg_6, arg_7, arg_8){//WebGL2.idr:502:1--504:1
 switch(arg_1.h){
  case 0: {
   return arg_1.a2(undefined)((eta_0) => {return WebGL2_prim$5f$5fvertexAttribPointer(arg_2, arg_3, arg_4, WebGL2_enumName$5fGLEnum$5f$5fAttribType(arg_5), JSFFI_asAnyPtr$5fJSNative$5f$5fBool(arg_6), arg_7, arg_8, eta_0);});
   break; }
  default:
   throw new Error('unhandled con case on Prelude/IO.idr:35:3--35:32');
 }
}

function WebGL2_useProgram(arg_0, arg_1, arg_2, arg_3){//WebGL2.idr:385:1--387:1
 switch(arg_1.h){
  case 0: {
   return arg_1.a2(undefined)((eta_0) => {return WebGL2_prim$5f$5fuseProgram(arg_2, arg_3, eta_0);});
   break; }
  default:
   throw new Error('unhandled con case on Prelude/IO.idr:35:3--35:32');
 }
}

function WebGL2_uniform1fv(arg_0, arg_1, arg_2, arg_3, arg_4, arg_5, arg_6){//WebGL2.idr:457:1--459:1
 switch(arg_1.h){
  case 0: {
   return arg_1.a2(undefined)((eta_0) => {return WebGL2_prim$5f$5funiform1fv(arg_2, arg_3, WebGL2_asAnyPtr$5fJSNative$5f$5fFloat32Array(arg_4), arg_5, arg_6, eta_0);});
   break; }
  default:
   throw new Error('unhandled con case on Prelude/IO.idr:35:3--35:32');
 }
}

function WebGL2_shaderSource(arg_0, arg_1, arg_2, arg_3, arg_4){//WebGL2.idr:376:1--378:1
 switch(arg_1.h){
  case 0: {
   return arg_1.a2(undefined)((eta_0) => {return WebGL2_prim$5f$5fshaderSource(arg_2, arg_3, arg_4, eta_0);});
   break; }
  default:
   throw new Error('unhandled con case on Prelude/IO.idr:35:3--35:32');
 }
}

const WebGL2_prim$5f$5fvertexAttribPointer = ( (gl, index, size, type, normalized, stride, offset) => gl.vertexAttribPointer(Number(index), Number(size), gl[type], normalized != 0, Number(stride), Number(offset)))


const WebGL2_prim$5f$5fuseProgram = ( (gl, program) => gl.useProgram(program))


const WebGL2_prim$5f$5funiform1fv = ( (gl, location, data, offset, length) => gl.uniform1fv(location, data, Number(offset), Number(length)))


const WebGL2_prim$5f$5fshaderSource = ( (gl, shader, source) => gl.shaderSource(shader, source))


const WebGL2_prim$5f$5fset$5fFloat32Array$5fat = ( (a, i, v) => a[Number(i)] = v)


const WebGL2_prim$5f$5fnew$5fFloat32Array$5fsize = ( size => new Float32Array(Number(size)))


const WebGL2_prim$5f$5flinkProgram = ( (gl, program) => gl.linkProgram(program))


const WebGL2_prim$5f$5fglClearColor = ( (gl, r, g, b, a) => gl.clearColor(r,g,b,a))


const WebGL2_prim$5f$5fglClear = ( gl => gl.clear(gl.COLOR_BUFFER_BIT))


const WebGL2_prim$5f$5fgetUniformLocation = ( (gl, program, name) => gl.getUniformLocation(program, name))


const WebGL2_prim$5f$5fgetShaderInfoLog = ( (gl, shader) => gl.getShaderInfoLog(shader))


const WebGL2_prim$5f$5fgetProgramInfoLog = ( (gl, program) => gl.getProgramInfoLog(program))


const WebGL2_prim$5f$5fgetContext = ((canvas, str) => canvas.getContext(str))


const WebGL2_prim$5f$5fgetAttribLocation = ( (gl, program, name) => gl.getAttribLocation(program, name))


const WebGL2_prim$5f$5fenableVertexAttribArray = ( (gl, index) => gl.enableVertexAttribArray(Number(index)))


const WebGL2_prim$5f$5fdrawArrays = ( (gl, mode, first, count) => gl.drawArrays(gl[mode], Number(first), Number(count)))


const WebGL2_prim$5f$5fdocument$5fquerySelector = (query=>document.querySelector(query))


const WebGL2_prim$5f$5fcreateShader = ( (gl, type) => gl.createShader(gl[type]))


const WebGL2_prim$5f$5fcreateProgram = ( gl => gl.createProgram())


const WebGL2_prim$5f$5fcreateBuffer = ( gl => gl.createBuffer())


const WebGL2_prim$5f$5fcompileShader = ( (gl, shader) => gl.compileShader(shader))


const WebGL2_prim$5f$5fbufferData = ( (gl, target, data, usage) => gl.bufferData(gl[target], data, gl[usage]))


const WebGL2_prim$5f$5fbindBuffer = ( (gl, targetName, buffer) => gl.bindBuffer(gl[targetName], buffer))


const WebGL2_prim$5f$5fattachShader = ( (gl, program, shader) => gl.attachShader(program, shader))


function WebGL2_linkProgram(arg_0, arg_1, arg_2, arg_3){//WebGL2.idr:367:1--369:1
 switch(arg_1.h){
  case 0: {
   return arg_1.a2(undefined)((eta_0) => {return WebGL2_prim$5f$5flinkProgram(arg_2, arg_3, eta_0);});
   break; }
  default:
   throw new Error('unhandled con case on Prelude/IO.idr:35:3--35:32');
 }
}

function WebGL2_initGL(arg_0, arg_1){//WebGL2.idr:27:1--28:34
 let imp$5fgen_37;
 switch(arg_1.h){
  case 0: {
   imp$5fgen_37 = arg_1.a1;
   break; }
  default:
   throw new Error('unhandled con case on Prelude/IO.idr:33:1--35:32');
 }
 switch(imp$5fgen_37.h){
  case 0: {
   let imp$5fgen_38;
   switch(arg_1.h){
    case 0: {
     imp$5fgen_38 = arg_1.a2(undefined)((eta_0) => {return WebGL2_prim$5f$5fdocument$5fquerySelector('#glCanvas', eta_0);});
     break; }
    default:
     throw new Error('unhandled con case on Prelude/IO.idr:35:3--35:32');
   }
   return imp$5fgen_37.a2(undefined)(undefined)(imp$5fgen_38)((canvas) => {let imp$5fgen_39;
switch(arg_1.h){
 case 0: {
  imp$5fgen_39 = arg_1.a1;
  break; }
 default:
  throw new Error('unhandled con case on Prelude/IO.idr:33:1--35:32');
}
switch(imp$5fgen_39.h){
 case 0: {
  let imp$5fgen_40;
  switch(arg_1.h){
   case 0: {
    imp$5fgen_40 = arg_1.a2(undefined)((eta_0) => {return WebGL2_prim$5f$5fgetContext(canvas, 'webgl2', eta_0);});
    break; }
   default:
    throw new Error('unhandled con case on Prelude/IO.idr:35:3--35:32');
  }
  return imp$5fgen_39.a2(undefined)(undefined)(imp$5fgen_40)((gl) => {let imp$5fgen_41;
switch(arg_1.h){
 case 0: {
  imp$5fgen_41 = arg_1.a1;
  break; }
 default:
  throw new Error('unhandled con case on Prelude/IO.idr:33:1--35:32');
}
let imp$5fgen_42;
switch(imp$5fgen_41.h){
 case 0: {
  imp$5fgen_42 = imp$5fgen_41.a1;
  break; }
 default:
  throw new Error('unhandled con case on Prelude/Interfaces.idr:92:1--104:1');
}
switch(imp$5fgen_42.h){
 case 0: {
  return imp$5fgen_42.a2(undefined)(gl);
  break; }
 default:
  throw new Error('unhandled con case on Prelude/Interfaces.idr:72:3--72:18');
}});
  break; }
 default:
  throw new Error('unhandled con case on Prelude/Interfaces.idr:95:3--95:35');
}});
   break; }
  default:
   throw new Error('unhandled con case on Prelude/Interfaces.idr:95:3--95:35');
 }
}

function WebGL2_getUniformLocation(arg_0, arg_1, arg_2, arg_3, arg_4){//WebGL2.idr:439:1--440:93
 let imp$5fgen_43;
 switch(arg_1.h){
  case 0: {
   imp$5fgen_43 = arg_1.a1;
   break; }
  default:
   throw new Error('unhandled con case on Prelude/IO.idr:33:1--35:32');
 }
 switch(imp$5fgen_43.h){
  case 0: {
   let imp$5fgen_44;
   switch(arg_1.h){
    case 0: {
     imp$5fgen_44 = arg_1.a2(undefined)((eta_0) => {return WebGL2_prim$5f$5fgetUniformLocation(arg_2, arg_3, arg_4, eta_0);});
     break; }
    default:
     throw new Error('unhandled con case on Prelude/IO.idr:35:3--35:32');
   }
   return imp$5fgen_43.a2(undefined)(undefined)(imp$5fgen_44)((uniformLocation) => {let imp$5fgen_45;
switch(arg_1.h){
 case 0: {
  imp$5fgen_45 = arg_1.a1;
  break; }
 default:
  throw new Error('unhandled con case on Prelude/IO.idr:33:1--35:32');
}
let imp$5fgen_46;
switch(imp$5fgen_45.h){
 case 0: {
  imp$5fgen_46 = imp$5fgen_45.a1;
  break; }
 default:
  throw new Error('unhandled con case on Prelude/Interfaces.idr:92:1--104:1');
}
switch(imp$5fgen_46.h){
 case 0: {
  return imp$5fgen_46.a2(undefined)(uniformLocation);
  break; }
 default:
  throw new Error('unhandled con case on Prelude/Interfaces.idr:72:3--72:18');
}});
   break; }
  default:
   throw new Error('unhandled con case on Prelude/Interfaces.idr:95:3--95:35');
 }
}

function WebGL2_getShaderInfoLog(arg_0, arg_1, arg_2, arg_3){//WebGL2.idr:345:1--346:66
 switch(arg_1.h){
  case 0: {
   return arg_1.a2(undefined)((eta_0) => {return WebGL2_prim$5f$5fgetShaderInfoLog(arg_2, arg_3, eta_0);});
   break; }
  default:
   throw new Error('unhandled con case on Prelude/IO.idr:35:3--35:32');
 }
}

function WebGL2_getProgramInfoLog(arg_0, arg_1, arg_2, arg_3){//WebGL2.idr:330:1--331:68
 switch(arg_1.h){
  case 0: {
   return arg_1.a2(undefined)((eta_0) => {return WebGL2_prim$5f$5fgetProgramInfoLog(arg_2, arg_3, eta_0);});
   break; }
  default:
   throw new Error('unhandled con case on Prelude/IO.idr:35:3--35:32');
 }
}

function WebGL2_getAttribLocation(arg_0, arg_1, arg_2, arg_3, arg_4){//WebGL2.idr:424:1--425:86
 let imp$5fgen_47;
 switch(arg_1.h){
  case 0: {
   imp$5fgen_47 = arg_1.a1;
   break; }
  default:
   throw new Error('unhandled con case on Prelude/IO.idr:33:1--35:32');
 }
 switch(imp$5fgen_47.h){
  case 0: {
   let imp$5fgen_48;
   switch(arg_1.h){
    case 0: {
     imp$5fgen_48 = arg_1.a2(undefined)((eta_0) => {return WebGL2_prim$5f$5fgetAttribLocation(arg_2, arg_3, arg_4, eta_0);});
     break; }
    default:
     throw new Error('unhandled con case on Prelude/IO.idr:35:3--35:32');
   }
   return imp$5fgen_47.a2(undefined)(undefined)(imp$5fgen_48)((aLoc) => {let imp$5fgen_49;
switch(arg_1.h){
 case 0: {
  imp$5fgen_49 = arg_1.a1;
  break; }
 default:
  throw new Error('unhandled con case on Prelude/IO.idr:33:1--35:32');
}
let imp$5fgen_50;
switch(imp$5fgen_49.h){
 case 0: {
  imp$5fgen_50 = imp$5fgen_49.a1;
  break; }
 default:
  throw new Error('unhandled con case on Prelude/Interfaces.idr:92:1--104:1');
}
switch(imp$5fgen_50.h){
 case 0: {
  return imp$5fgen_50.a2(undefined)(aLoc);
  break; }
 default:
  throw new Error('unhandled con case on Prelude/Interfaces.idr:72:3--72:18');
}});
   break; }
  default:
   throw new Error('unhandled con case on Prelude/Interfaces.idr:95:3--95:35');
 }
}

function WebGL2_float32Array(arg_0, arg_1, arg_2, arg_3){//WebGL2.idr:155:1--156:77
 let imp$5fgen_51;
 switch(arg_1.h){
  case 0: {
   imp$5fgen_51 = arg_1.a1;
   break; }
  default:
   throw new Error('unhandled con case on Prelude/IO.idr:33:1--35:32');
 }
 switch(imp$5fgen_51.h){
  case 0: {
   let imp$5fgen_52;
   switch(arg_1.h){
    case 0: {
     imp$5fgen_52 = arg_1.a2(undefined)((eta_0) => {return WebGL2_prim$5f$5fnew$5fFloat32Array$5fsize(Prelude_Types_cast$5fCast$5f$5fNat$5fInt(arg_2), eta_0);});
     break; }
    default:
     throw new Error('unhandled con case on Prelude/IO.idr:35:3--35:32');
   }
   return imp$5fgen_51.a2(undefined)(undefined)(imp$5fgen_52)((typedArrayAnyPtr) => {let imp$5fgen_53;
switch(arg_1.h){
 case 0: {
  imp$5fgen_53 = arg_1.a1;
  break; }
 default:
  throw new Error('unhandled con case on Prelude/IO.idr:33:1--35:32');
}
switch(imp$5fgen_53.h){
 case 0: {
  return imp$5fgen_53.a2(undefined)(undefined)(WebGL2_fill$5fFloat32Array(undefined, undefined, arg_1, typedArrayAnyPtr, arg_3))(($5f_2058) => {let imp$5fgen_54;
switch(arg_1.h){
 case 0: {
  imp$5fgen_54 = arg_1.a1;
  break; }
 default:
  throw new Error('unhandled con case on Prelude/IO.idr:33:1--35:32');
}
let imp$5fgen_55;
switch(imp$5fgen_54.h){
 case 0: {
  imp$5fgen_55 = imp$5fgen_54.a1;
  break; }
 default:
  throw new Error('unhandled con case on Prelude/Interfaces.idr:92:1--104:1');
}
switch(imp$5fgen_55.h){
 case 0: {
  return imp$5fgen_55.a2(undefined)(typedArrayAnyPtr);
  break; }
 default:
  throw new Error('unhandled con case on Prelude/Interfaces.idr:72:3--72:18');
}});
  break; }
 default:
  throw new Error('unhandled con case on Prelude/Interfaces.idr:95:3--95:35');
}});
   break; }
  default:
   throw new Error('unhandled con case on Prelude/Interfaces.idr:95:3--95:35');
 }
}

function WebGL2_fill$5fFloat32Array(arg_0, arg_1, arg_2, arg_3, ext_0){//WebGL2.idr:144:1--145:1
 return WebGL2_n__2567_1957_fill$5fFloat32Array$5frecursive(undefined, undefined, arg_2, arg_3, undefined, 0n, ext_0);
}

function WebGL2_enableVertexAttribArray(arg_0, arg_1, arg_2, arg_3){//WebGL2.idr:411:1--413:1
 switch(arg_1.h){
  case 0: {
   return arg_1.a2(undefined)((eta_0) => {return WebGL2_prim$5f$5fenableVertexAttribArray(arg_2, arg_3, eta_0);});
   break; }
  default:
   throw new Error('unhandled con case on Prelude/IO.idr:35:3--35:32');
 }
}

function WebGL2_drawArrays(arg_0, arg_1, arg_2, arg_3, arg_4, arg_5){//WebGL2.idr:539:1--541:1
 switch(arg_1.h){
  case 0: {
   return arg_1.a2(undefined)((eta_0) => {return WebGL2_prim$5f$5fdrawArrays(arg_2, WebGL2_enumName$5fGLEnum$5f$5fDrawMode(arg_3), arg_4, arg_5, eta_0);});
   break; }
  default:
   throw new Error('unhandled con case on Prelude/IO.idr:35:3--35:32');
 }
}

function WebGL2_createShader(arg_0, arg_1, arg_2, arg_3){//WebGL2.idr:289:1--290:68
 let imp$5fgen_56;
 switch(arg_1.h){
  case 0: {
   imp$5fgen_56 = arg_1.a1;
   break; }
  default:
   throw new Error('unhandled con case on Prelude/IO.idr:33:1--35:32');
 }
 switch(imp$5fgen_56.h){
  case 0: {
   let imp$5fgen_57;
   switch(arg_1.h){
    case 0: {
     imp$5fgen_57 = arg_1.a2(undefined)((eta_0) => {return WebGL2_prim$5f$5fcreateShader(arg_2, WebGL2_enumName$5fGLEnum$5f$5fGLShaderType(arg_3), eta_0);});
     break; }
    default:
     throw new Error('unhandled con case on Prelude/IO.idr:35:3--35:32');
   }
   return imp$5fgen_56.a2(undefined)(undefined)(imp$5fgen_57)((shader) => {let imp$5fgen_58;
switch(arg_1.h){
 case 0: {
  imp$5fgen_58 = arg_1.a1;
  break; }
 default:
  throw new Error('unhandled con case on Prelude/IO.idr:33:1--35:32');
}
let imp$5fgen_59;
switch(imp$5fgen_58.h){
 case 0: {
  imp$5fgen_59 = imp$5fgen_58.a1;
  break; }
 default:
  throw new Error('unhandled con case on Prelude/Interfaces.idr:92:1--104:1');
}
switch(imp$5fgen_59.h){
 case 0: {
  return imp$5fgen_59.a2(undefined)(shader);
  break; }
 default:
  throw new Error('unhandled con case on Prelude/Interfaces.idr:72:3--72:18');
}});
   break; }
  default:
   throw new Error('unhandled con case on Prelude/Interfaces.idr:95:3--95:35');
 }
}

function WebGL2_createProgram(arg_0, arg_1, arg_2){//WebGL2.idr:278:1--279:54
 let imp$5fgen_60;
 switch(arg_1.h){
  case 0: {
   imp$5fgen_60 = arg_1.a1;
   break; }
  default:
   throw new Error('unhandled con case on Prelude/IO.idr:33:1--35:32');
 }
 switch(imp$5fgen_60.h){
  case 0: {
   let imp$5fgen_61;
   switch(arg_1.h){
    case 0: {
     imp$5fgen_61 = arg_1.a2(undefined)((eta_0) => {return WebGL2_prim$5f$5fcreateProgram(arg_2, eta_0);});
     break; }
    default:
     throw new Error('unhandled con case on Prelude/IO.idr:35:3--35:32');
   }
   return imp$5fgen_60.a2(undefined)(undefined)(imp$5fgen_61)((program) => {let imp$5fgen_62;
switch(arg_1.h){
 case 0: {
  imp$5fgen_62 = arg_1.a1;
  break; }
 default:
  throw new Error('unhandled con case on Prelude/IO.idr:33:1--35:32');
}
let imp$5fgen_63;
switch(imp$5fgen_62.h){
 case 0: {
  imp$5fgen_63 = imp$5fgen_62.a1;
  break; }
 default:
  throw new Error('unhandled con case on Prelude/Interfaces.idr:92:1--104:1');
}
switch(imp$5fgen_63.h){
 case 0: {
  return imp$5fgen_63.a2(undefined)(program);
  break; }
 default:
  throw new Error('unhandled con case on Prelude/Interfaces.idr:72:3--72:18');
}});
   break; }
  default:
   throw new Error('unhandled con case on Prelude/Interfaces.idr:95:3--95:35');
 }
}

function WebGL2_createBuffer(arg_0, arg_1, arg_2){//WebGL2.idr:211:1--212:55
 let imp$5fgen_64;
 switch(arg_1.h){
  case 0: {
   imp$5fgen_64 = arg_1.a1;
   break; }
  default:
   throw new Error('unhandled con case on Prelude/IO.idr:33:1--35:32');
 }
 switch(imp$5fgen_64.h){
  case 0: {
   let imp$5fgen_65;
   switch(arg_1.h){
    case 0: {
     imp$5fgen_65 = arg_1.a2(undefined)((eta_0) => {return WebGL2_prim$5f$5fcreateBuffer(arg_2, eta_0);});
     break; }
    default:
     throw new Error('unhandled con case on Prelude/IO.idr:35:3--35:32');
   }
   return imp$5fgen_64.a2(undefined)(undefined)(imp$5fgen_65)((buffer) => {let imp$5fgen_66;
switch(arg_1.h){
 case 0: {
  imp$5fgen_66 = arg_1.a1;
  break; }
 default:
  throw new Error('unhandled con case on Prelude/IO.idr:33:1--35:32');
}
let imp$5fgen_67;
switch(imp$5fgen_66.h){
 case 0: {
  imp$5fgen_67 = imp$5fgen_66.a1;
  break; }
 default:
  throw new Error('unhandled con case on Prelude/Interfaces.idr:92:1--104:1');
}
switch(imp$5fgen_67.h){
 case 0: {
  return imp$5fgen_67.a2(undefined)(buffer);
  break; }
 default:
  throw new Error('unhandled con case on Prelude/Interfaces.idr:72:3--72:18');
}});
   break; }
  default:
   throw new Error('unhandled con case on Prelude/Interfaces.idr:95:3--95:35');
 }
}

function WebGL2_compileShader(arg_0, arg_1, arg_2, arg_3){//WebGL2.idr:269:1--271:1
 switch(arg_1.h){
  case 0: {
   return arg_1.a2(undefined)((eta_0) => {return WebGL2_prim$5f$5fcompileShader(arg_2, arg_3, eta_0);});
   break; }
  default:
   throw new Error('unhandled con case on Prelude/IO.idr:35:3--35:32');
 }
}

function WebGL2_clearColor(arg_0, arg_1, arg_2, arg_3, arg_4, arg_5, arg_6){//WebGL2.idr:38:1--40:1
 switch(arg_1.h){
  case 0: {
   return arg_1.a2(undefined)((eta_0) => {return WebGL2_prim$5f$5fglClearColor(arg_2, arg_3, arg_4, arg_5, arg_6, eta_0);});
   break; }
  default:
   throw new Error('unhandled con case on Prelude/IO.idr:35:3--35:32');
 }
}

function WebGL2_clear(arg_0, arg_1, arg_2){//WebGL2.idr:46:1--48:1
 switch(arg_1.h){
  case 0: {
   return arg_1.a2(undefined)((eta_0) => {return WebGL2_prim$5f$5fglClear(arg_2, eta_0);});
   break; }
  default:
   throw new Error('unhandled con case on Prelude/IO.idr:35:3--35:32');
 }
}

function WebGL2_bufferData(arg_0, arg_1, arg_2, arg_3, arg_4, arg_5, arg_6, arg_7){//WebGL2.idr:191:1--193:1
 switch(arg_2.h){
  case 0: {
   return arg_2.a2(undefined)((eta_0) => {return WebGL2_prim$5f$5fbufferData(arg_4, WebGL2_enumName$5fGLEnum$5f$5fGLBufferTarget(arg_5), arg_3(arg_6), WebGL2_enumName$5fGLEnum$5f$5fGLBufferUsage(arg_7), eta_0);});
   break; }
  default:
   throw new Error('unhandled con case on Prelude/IO.idr:35:3--35:32');
 }
}

function WebGL2_bindBuffer(arg_0, arg_1, arg_2, arg_3, arg_4){//WebGL2.idr:113:1--115:1
 switch(arg_1.h){
  case 0: {
   return arg_1.a2(undefined)((eta_0) => {return WebGL2_prim$5f$5fbindBuffer(arg_2, WebGL2_enumName$5fGLEnum$5f$5fGLBufferTarget(arg_3), arg_4, eta_0);});
   break; }
  default:
   throw new Error('unhandled con case on Prelude/IO.idr:35:3--35:32');
 }
}

function WebGL2_attachShader(arg_0, arg_1, arg_2, arg_3, arg_4){//WebGL2.idr:256:1--258:1
 switch(arg_1.h){
  case 0: {
   return arg_1.a2(undefined)((eta_0) => {return WebGL2_prim$5f$5fattachShader(arg_2, arg_3, arg_4, eta_0);});
   break; }
  default:
   throw new Error('unhandled con case on Prelude/IO.idr:35:3--35:32');
 }
}
try{PrimIO_unsafePerformIO(undefined, (eta_0) => {return Main_main(eta_0);});}catch(e){if(e instanceof IdrisError){console.log('ERROR: ' + e.message)}else{throw e} }