---
title: Haskell for madmen
cover_image: haskell-for-madmen.jpg
link_top: ..
---

_If you spot a mistake, grammar error or unclear passage, please please open an
issue or make a merge request over on my [gitlab repository](https://gitlab.com/DrBearhands/blog)
and help me make this tutorial better._

# Introduction

There appears to be some interest in a Haskell tutorial that does not take
things slowly, but rather dives right into creating a real-world application,
specifically a webserver. In order to do this though, we will need to use monads
right from the get-go, and monads are traditionally considered hard.

So I decided to write the tutorial I wished I had in university. It explains the
*why*, not just the *how*, and it goes pretty bat-shit fast. No taking it slow
with the repl here. This tutorial holds you hand, but speeds on the highway
while doing so.

And so this tutorial is born: Haskell for madmen.

This tutorial works best with Firefox, as some formulas are written in MathML,
but you should be able to make do without them.

# Acknowledgments

Special thanks to the following people for their contributions:

* [Rixt Heerschop](https://www.rixtheerschop.com/) (cover art)
* [Michael Kohl](https://dev.to/citizen428) (proofreading)

# Index

* [Setup](1.setup)
* [Hello, monad!](2.hello-monad)
* [Hello, web server!](3.hello-server)
* [Types of TODO](4.types-of-todo)
* [Connecting to a database](5.connecting-to-db)
* [Reflections and cleanup](6.reflection-and-cleanup)
* [Contravariant input](7.contravariant-input)
* [Wrapping up and closing remarks](8.conclusion)
