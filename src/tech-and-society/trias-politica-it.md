---
title: The trias politica needs an IT branch
author: DrBearhands
description:
tags: #discuss #opinion #politics #governance
---

COVID19 apps have brought privacy concerns into the public's eye once more. Sacrificing privacy to curb the spread rate of deadly diseases sounds like a good idea in principle, but it is a false dichotomy. The real choice is: privacy, utility (monitoring potential infections in this case), concentration of power in the executive government, choose 2.

Contact tracing can be done entirely through computers, without humans. Getting the executive government involved is a choice. A choice most of us aren't even aware exists.

The COVID19 app is not a special case either, it is the norm. Taxes, criminal records, vehicle registration, and semi-public services like healthcare, postal services and insurance... all require *correct* handling of sensitive data. The issue is not that data is stored somewhere, the issue is that the data needs to be used according to the stated purpose and *only* for the stated purpose.

------------------------

Innovations in information technology have caused control over data to become a real form of power. It's what allowed Google and Facebook to become some of the richest companies in the world.

Separation of such powers is at the center of any democratic government. Unfortunately, the [trias politica](https://en.wikipedia.org/wiki/Separation_of_powers) is too old to consider the power of modern information technology. Yet the problems we see today from data management being handled by the executive government are, in essence, the same that would arise if the judicial power and executive power were merged into one branch.
In Montesquieu's writing:

> Again, there is no liberty, if the judiciary power be not separated from the legislative and executive. Were it joined with the legislative, the life and liberty of the subject would be exposed to arbitrary control; for the judge would be then the legislator. Were it joined to the executive power, the judge might behave with violence and oppression.

The argument still holds if we replace "judge" with "data admin". A legislator with control over data may alter said data, or create data-driven legislation, in such a way as to act as a de-facto judge, or to put unjust and unethical burden on specific groups or individuals. Similarly, an executive branch with data control may consult and alter the data to justify, hide and facilitate tyrannical behavior.

We do, however, also need to separate the judiciary from the data admins for a practical reason: it would be untenable to require the judiciary to have both an education in law, and an education in computer science. On the other hand, judges without an education in law would endanger the right to a fair trial, and data admins with no education in computer science would endanger the security of data storage. Of course, the same issue around education / competence arise if we were to assign data administrative powers to executive or legislative powers, and this is the source of most of the problems we have been facing thus far.

---------------

Imagine that we had the same blatant disregard for the judicial branch that we show information technology within our governments. Judges subordinate to an executive branch that has no knowledge of the subject matter, each ministry using their own judges, without precedential opinions, contracted to private companies who may subcontract to cheap judges in India fresh out of judge bootcamp... that's the situation we're in.

---------------

What we need is a fourth branch of government, separate from judiciary, executive and legislative branches, charged with the task of maintaining effective and transparent data management.

Such a branch would set up databases, log data access, run functions that need access to many data point, ensure that used technology implements the intention of the law and dictate what systems may be used by government agencies and push for the development of new, open technology when it is missing. Such a branch must, of course, operate with the highest transparency.

---------------

We have mostly been spared the tyranny arising from the conjunction of data administrative and executive or legislative powers due to the general incompetence of politicians. Nevertheless, a few cases relating to ill intent rather than incompetence have already been exposed:

- Cambridge Analytica
- Use of NHS data to track immigrants
- The misuse of the 2020 US census by including a citizenship question
- Chinese social credit
- Snowden's revelations
- The Stasi
- The Nazi use of population records in occupied territories to identify Jews

I can only expect these problems to increase as (third-party) competence, and understanding of how lucrative data abuse can be, reaches politicians.

I should underline that, as shown in the examples above, this is an issue of record-keeping, not necessarily one of computers. The rise of IT and AI has, however, made the issues far more important, better hidden, yet perhaps solvable for the first time in history.
