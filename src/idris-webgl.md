---
title: Making WebGL typesafe in Idris 2
link_top: ..
---

I'd wanted to learn Idris for a while, and see how far I could realistically take type-safety. In particular, can I ensure all documentation of the WebGL spec is encompassed in types? (thus achieving [correctness-by-construction](https://www.youtube.com/watch?v=03mUs5NlT6U)).

The writing is not particularly well curated. This a log of my thoughts while learning and developing. Views and opinions will change throughout the chapters. I will probably be wrong often.

* [1: Hello triangle, first thoughts](part1)
* [2: Some animation](part2)
* [3: Preliminary performance test](part3)
* [4: Troubles with linearity](part4)
* [5: Linearity continued](part5)
* [6: Bye IO monad, hello GL monad](part6)
* [7: Short code quality update](part7)
* [8: Baby's first dependent type](part8)
* [9: Ensuring uniforms belong to bound program](part9)
* [10: Implicit arguments &amp; monad transformers](part10)
* [11: No linearity with monadic errors (for now)](part11)
* [12: Linear algebra with linear types... not great](part12)
* [13: Slow and frustrating progress](part13)
* [14: Getting back into it](part14)
* [15: Restricting arguments to a list](part15)
* [16: Binding programs again](part16)
* [17: A Hoare state failure](part17)
* [18: Hiatus](part18)