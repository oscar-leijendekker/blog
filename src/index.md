---
title: DrBearhands' writings

---

## Blog posts
* [The glTF file format for 3D models has some issues](articles/20241219-gltf)
* [Web Monetization debunked](articles/webmonetization-debunked)
* [On automated versioning strategies for CI/CD pipelines](articles/cicd-versioning)
* [Prematurely hand-optimizing C++ code for shits and giggles](articles/hand-optimized-c)
* [The hardest thing I ever did: Hoare state monads](articles/hoare-monads)
* [The trias politica needs an IT branch](tech-and-society/trias-politica-it)

## Tutorials
* [Haskell for madmen](haskell-tutorial)

## Development logs
* [Making WebGL typesafe in Idris 2](idris-webgl)
* Making a hollistic programming language
  * [Planning](pl-design/1-planning)
  * [The Abstract Syntax Tree](pl-design/2-ast)