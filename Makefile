PANDOC=pandoc
SRC_DIR=src
PFLAGS=-t html5 -r markdown -s --template=$(SRC_DIR)/blog_template -c "/style/pandoc.css"
PFLAG_MATHML=--mathml
PFLAG_MATH=--mathjax=https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js
OUT_DIR=public

SRC_MD_FILES=$(filter-out $(SRC_DIR)/index.md, $(shell find $(SRC_DIR)/ -type f -name "*.md") )
OUT_MD_FILES=$(patsubst $(SRC_DIR)/%.md,$(OUT_DIR)/%/index.html,$(SRC_MD_FILES))

SRC_CSS=$(shell find $(SRC_DIR)/ -type f -name '*.css')
OUT_CSS=$(patsubst $(SRC_DIR)/%.css,$(OUT_DIR)/%.css,$(SRC_CSS))
SRC_PNG=$(shell find $(SRC_DIR)/ -type f -name '*.png')
OUT_PNG=$(patsubst $(SRC_DIR)/%.png,$(OUT_DIR)/%.png,$(SRC_PNG))
SRC_JPG=$(shell find $(SRC_DIR)/ -type f -name '*.jpg')
OUT_JPG=$(patsubst $(SRC_DIR)/%.jpg,$(OUT_DIR)/%.jpg,$(SRC_JPG))
SRC_SVG=$(shell find $(SRC_DIR)/ -type f -name '*.svg')
OUT_SVG=$(patsubst $(SRC_DIR)/%.svg,$(OUT_DIR)/%.svg,$(SRC_SVG))
SRC_JS=$(shell find $(SRC_DIR)/ -type f -name '*.js')
OUT_JS=$(patsubst $(SRC_DIR)/%.js,$(OUT_DIR)/%.js,$(SRC_JS))

.phony: all clean

all: $(OUT_MD_FILES) $(OUT_CSS) $(OUT_PNG) $(OUT_JPG) $(OUT_SVG) $(OUT_JS) $(OUT_DIR)/index.html 

clean:
	rm -rf $(OUT_DIR)

$(OUT_DIR)/.well-known/:
	mkdir $(OUT_DIR)/.well-known

$(OUT_DIR)/index.html: $(SRC_DIR)/index.md $(SRC_DIR)/blog_template.html5
	mkdir -p $(shell dirname $@)
	$(PANDOC) $(PFLAGS) $(PFLAG_MATH) -o $@ $<

$(OUT_DIR)/%/index.html: $(SRC_DIR)/%.md $(SRC_DIR)/blog_template.html5
	mkdir -p $(shell dirname $@)
	$(PANDOC) $(PFLAGS) $(PFLAG_MATH) -o $@ $<

$(OUT_DIR)/%.css: $(SRC_DIR)/%.css
	mkdir -p $(shell dirname $@)
	cp $< $@

$(OUT_DIR)/%.png: $(SRC_DIR)/%.png
	mkdir -p $(shell dirname $@)
	cp $< $@

$(OUT_DIR)/%.jpg: $(SRC_DIR)/%.jpg
	mkdir -p $(shell dirname $@)
	cp $< $@

$(OUT_DIR)/%.svg: $(SRC_DIR)/%.svg
	mkdir -p $(shell dirname $@)
	cp $< $@

$(OUT_DIR)/%.js: $(SRC_DIR)/%.js
	mkdir -p $(shell dirname $@)
	cp $< $@
